/*
 * Copyright (C) 2008-2018 TrinityCore <https://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "Object.h"
#include "MapObject.h"

enum SceneType : uint32
{
    SCENE_TYPE_NONE         = 0,
    SCENE_TYPE_BATTLEPET    = 1
};

class TC_GAME_API SceneObject : public WorldObject, public GridObject<SceneObject>, public MapObject
{
public:
    SceneObject(bool isWorldObject = false);
    virtual ~SceneObject();

    static SceneObject* CreateSceneObject(uint32 scriptPackageID, Unit* creator, Position const& pos, SceneType type = SCENE_TYPE_NONE, SpellInfo const* spellInfo = nullptr);
    bool Create(ObjectGuid::LowType lowGuid, uint32 scriptPackageID, Map* map, Unit* creator, Position const& pos, SceneType type = SCENE_TYPE_NONE, SpellInfo const* spellInfo = nullptr);

    void AddToWorld() override;
    void RemoveFromWorld() override;
    void Remove();
    void BindToCreator();
    void UnbindFromCreator();
    ObjectGuid const& GetCreatorGUID() const { return GetGuidValue(SCENEOBJECT_FIELD_CREATEDBY); }
    void Update(uint32 diff) override;
    Unit* GetCreator() const { return _creator; }
    bool IsCreatedBySpell() const { return _spellInfo != nullptr; }
    std::string const& GetLocalScript() const;

protected:
    Unit* _creator;
    int32 _duration;
    SpellInfo const* _spellInfo;

    static int32 const InfiniteDuration;
};

#endif // SCENEOBJECT_H
