/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef DamageHistory_h__
#define DamageHistory_h__

#include "Define.h"
#include <map>

class TC_GAME_API DamageHistory
{
public:
    DamageHistory() : _lastCleanup(0) { }

    void AddDamageTaken(uint64 damage);
    uint64 CountDamageTaken(time_t startTime) const;
    time_t GetLastCleanup() const { return _lastCleanup; }

    void Cleanup(time_t now);

    static uint32 const CleanupIntervall;

private:
    time_t _lastCleanup;

    typedef std::multimap<time_t, uint64> StorageType;
    StorageType _takenData;
};


#endif // DamageHistory_h__
