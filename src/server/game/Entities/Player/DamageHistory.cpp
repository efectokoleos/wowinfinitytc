/*
 * Copyright (C) 2012-2017 OMFG.GG <https://www.omfg.gg/>
 *
 * This file is free software; as a special exception the author gives
 * unlimited permission to copy and/or distribute it, with or without
 * modifications, as long as this notice is preserved.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "DamageHistory.h"
#include "World.h"
#include <numeric>

uint32 const DamageHistory::CleanupIntervall = 60;

void DamageHistory::AddDamageTaken(uint64 damage)
{
    _takenData.emplace(sWorld->GetGameTime(), damage);
}

uint64 DamageHistory::CountDamageTaken(time_t startTime) const
{
    auto itr = _takenData.lower_bound(startTime);
    return std::accumulate(itr, _takenData.end(), uint64(0), [](uint64 damageTaken, StorageType::value_type const& pair)
    {
        return damageTaken += pair.second;
    });
}

void DamageHistory::Cleanup(time_t now)
{
    _lastCleanup = now;

    auto itr = _takenData.lower_bound(_lastCleanup - CleanupIntervall);
    _takenData.erase(_takenData.begin(), itr);
}
