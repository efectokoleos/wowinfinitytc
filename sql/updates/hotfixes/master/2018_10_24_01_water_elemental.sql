DELETE FROM `spell_effect` WHERE `ID` = 2910;
INSERT INTO `spell_effect` (`ID`, `Coefficient`, `SpellID`) VALUES 
(2910, 0.5, 21340); -- this hotfix removes the variance value from db2 which is incorrect
