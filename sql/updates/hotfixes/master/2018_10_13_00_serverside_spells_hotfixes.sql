-- Current max ids: spell: 300005, Effect: 500007
-- Paladin spells

DELETE FROM `spell` WHERE `ID` = 61988;
INSERT INTO `spell` (`Name`, `ID`) VALUES 
('Divine Shield Exclude Aura', 61988);

DELETE FROM `spell_misc` WHERE `ID` = 500000;
INSERT INTO `spell_misc` (`ID`, `Attributes1`, `Attributes2`, `Attributes3`, `Attributes4`, `CastingTimeIndex`, `DurationIndex`, `RangeIndex`) VALUES 
(500000, 67108864, 268436480, 4, 269484032, 1, 25, 13); 

DELETE FROM `spell_effect` WHERE `ID` = 500000;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectAura`, `ImplicitTarget1`, `SpellID`) VALUES 
(500000, 6, 4, 1, 61988); 

-- Mage spells

DELETE FROM `spell` WHERE `ID` = 300000;
INSERT INTO `spell` (`Name`, `ID`) VALUES 
('Brain Freeze: Pct damage mod holder', 300000);

DELETE FROM `spell_misc` WHERE `ID` = 500001;
INSERT INTO `spell_misc` (`ID`, `Attributes1`, `Attributes2`, `Attributes3`, `Attributes4`, `CastingTimeIndex`, `DurationIndex`, `RangeIndex`) VALUES 
(500001, 67108864, 268436480, 4, 269484032, 1, 39, 13); 

DELETE FROM `spell_effect` WHERE `ID` = 500001;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectAura`, `EffectBasePoints`, `ImplicitTarget1`, `SpellID`) VALUES 
(500001, 6, 4, 50, 1, 300000);

-- set spell info for Ignite (197409), server-side spell
SET @SpellId  := 197409;
SET @EffectId := 1000001;

-- Duration: 10000, Range: 40 yds, Icon: spell_fire_incinerate
DELETE FROM `spell` WHERE `ID` = @SpellId;
INSERT INTO `spell` (`ID`, `VerifiedBuild`) VALUES
(@SpellId, 22810);

-- SPELL_EFFECT_DUMMY (3) > Max Radius (Id 14) 8,00 > Targets (87, 16) (TARGET_DEST_DEST, TARGET_UNIT_DEST_AREA_ENEMY)
DELETE FROM `spell_effect` WHERE `ID` = @EffectId;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectRadiusIndex2`, `ImplicitTarget1`, `ImplicitTarget2`, `SpellID`, `EffectIndex`, `VerifiedBuild`) VALUES
(@EffectId, 3, 14, 87, 16, @SpellId, 0, 22810);

DELETE FROM `spell` WHERE `ID` = 300002;
INSERT INTO `spell` (`Name`, `ID`) VALUES 
('Nether Tempest: Damage holder', 300002);

DELETE FROM `spell_misc` WHERE `ID` = 500003;
INSERT INTO `spell_misc` (`ID`, `Attributes1`, `Attributes2`, `Attributes3`, `Attributes4`, `CastingTimeIndex`, `DurationIndex`, `RangeIndex`) VALUES 
(500003, 67108864, 268436480, 4, 269484032, 1, 305, 5); 

DELETE FROM `spell_effect` WHERE `ID` = 500003;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectAura`, `EffectBasePoints`, `ImplicitTarget1`, `SpellID`) VALUES 
(500003, 6, 4, 0, 6, 300002);

DELETE FROM `spell` WHERE `ID` = 300004;
INSERT INTO `spell` (`Name`, `ID`) VALUES 
('Hot Streak: ignite pyroblast helper (1000 ms duration)', 300004);

DELETE FROM `spell_misc` WHERE `ID` = 500006;
INSERT INTO `spell_misc` (`ID`, `Attributes1`, `Attributes2`, `Attributes3`, `Attributes4`, `CastingTimeIndex`, `DurationIndex`, `RangeIndex`) VALUES 
(500006, 67108864, 268436480, 4, 269484032, 1, 36, 13); 

DELETE FROM `spell_effect` WHERE `ID` = 500006;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectAura`, `ImplicitTarget1`, `SpellID`) VALUES 
(500006, 6, 4, 1, 300004); 

DELETE FROM `spell` WHERE `ID` = 300005;
INSERT INTO `spell` (`Name`, `ID`) VALUES 
('Hot Streak: ignite flamestrike helper (500 ms duration)', 300005);

DELETE FROM `spell_misc` WHERE `ID` = 500007;
INSERT INTO `spell_misc` (`ID`, `Attributes1`, `Attributes2`, `Attributes3`, `Attributes4`, `CastingTimeIndex`, `DurationIndex`, `RangeIndex`) VALUES 
(500007, 67108864, 268436480, 4, 269484032, 1, 327, 13); 

DELETE FROM `spell_effect` WHERE `ID` = 500007;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectAura`, `ImplicitTarget1`, `SpellID`) VALUES 
(500007, 6, 4, 1, 300005); 

-- Warlock spells
DELETE FROM `spell` WHERE `ID` = 62388;
INSERT INTO `spell` (`Name`, `ID`) VALUES 
('Demonic Circle: Teleport(48020) - casterAuraSpell', 62388);

DELETE FROM `spell_misc` WHERE `ID` = 500004;
INSERT INTO `spell_misc` (`ID`, `CastingTimeIndex`, `DurationIndex`, `RangeIndex`) VALUES 
(500004, 1, 21, 1); 

DELETE FROM `spell_effect` WHERE `ID` = 500004;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectAura`, `ImplicitTarget1`, `SpellID`) VALUES 
(500004, 6, 4, 1, 62388);

-- wild imp
DELETE FROM `spell` WHERE `ID` = 300003;
INSERT INTO `spell` (`Name`, `ID`) VALUES 
('Warlock: Wild Imp target marker (500ms duration)', 300003);

DELETE FROM `spell_misc` WHERE `ID` = 500005;
INSERT INTO `spell_misc` (`ID`, `Attributes1`, `Attributes2`, `Attributes3`, `Attributes4`, `CastingTimeIndex`, `DurationIndex`, `RangeIndex`) VALUES 
(500005, 67108864, 268436480, 4, 269484032, 1, 327, 5); 

DELETE FROM `spell_effect` WHERE `ID` = 500005;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectAura`, `EffectBasePoints`, `ImplicitTarget1`, `SpellID`) VALUES 
(500005, 6, 4, 5, 6, 300003);

-- generic spells

DELETE FROM `spell` WHERE `ID` = 300001;
INSERT INTO `spell` (`Name`, `ID`) VALUES 
('combo point holder (5cp) target enemy (100ms duration)', 300001);

DELETE FROM `spell_misc` WHERE `ID` = 500002;
INSERT INTO `spell_misc` (`ID`, `Attributes1`, `Attributes2`, `Attributes3`, `Attributes4`, `CastingTimeIndex`, `DurationIndex`, `RangeIndex`) VALUES 
(500002, 67108864, 268436480, 4, 269484032, 1, 407, 5); 

DELETE FROM `spell_effect` WHERE `ID` = 500002;
INSERT INTO `spell_effect` (`ID`, `Effect`, `EffectAura`, `EffectBasePoints`, `ImplicitTarget1`, `SpellID`) VALUES 
(500002, 6, 4, 5, 6, 300001);
